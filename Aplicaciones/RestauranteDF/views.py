from django.shortcuts import render,redirect
from .models import Provincia
from .models import Clientes,Pedido, Tipo, Platillo, Detalle, Ingredientes, Receta
from django.contrib import messages
# Create your views here.
#definir una function
def listadoProvincia(request):
    provinciasBdd=Provincia.objects.all()
    return render(request,'listadoProvincia.html',{'provincias':provinciasBdd})


def guardarProvincia(request):
    regionPr_df=request.POST["regionPr_df"]
    nombrePr_df=request.POST["nombrePr_df"]
    capitalPr_df=request.POST["capitalPr_df"]
    fotografia=request.FILES.get("fotografia")

    #insertando datos mediante el ORM de Django
    nuevaProvincia=Provincia.objects.create(regionPr_df=regionPr_df,nombrePr_df=nombrePr_df,capitalPr_df=capitalPr_df, fotografia=fotografia)
    messages.success(request,'Provincia guardado exitosamente');
    return redirect('/')

def eliminarProvincia(request,id_df):
    provinciaEliminar=Provincia.objects.get(id_df=id_df)
    provinciaEliminar.delete()
    messages.success(request,'Eliminado Exitosamente');
    return redirect('/')

def editarProvincia(request,id_df):
    provinciaEditar=Provincia.objects.get(id_df=id_df)
    return render(request,'editarProvincia.html' ,{'provincia': provinciaEditar})


def procesarActualizacionProvincia(request):
    id_df=request.POST["id_df"]
    regionPr_df=request.POST["regionPr_df"]
    nombrePr_df=request.POST["nombrePr_df"]
    capitalPr_df=request.POST["capitalPr_df"]
    if 'fotografia' in request.FILES:
        fotografia = request.FILES.get("fotografia")
    else:
            # Si no se proporciona una nueva imagen, conserva la existente
            provincia_existente = Provincia.objects.get(id_df=id_df)
            fotografia = provincia_existente.fotografia

    #Insertando datos mediante el ORM de DJANGO
    provinciaEditar=Provincia.objects.get(id_df=id_df)
    provinciaEditar.regionPr_df=regionPr_df
    provinciaEditar.nombrePr_df=nombrePr_df
    provinciaEditar.capitalPr_df=capitalPr_df
    provinciaEditar.fotografia=fotografia
    provinciaEditar.save()
    messages.success(request,
      'Provincia ACTUALIZADA Exitosamente')
    return redirect('/')

    #Clientes
def listaClientes(request):
    clienteBdd=Clientes.objects.all()
    provinciasBdd=Provincia.objects.all()
    return render(request,'listaClientes.html',{'clientes':clienteBdd, 'provincias':provinciasBdd})


def guardarCliente(request):
    #Capturando los valores del formulario
    codigo_pro=request.POST["codigo_pro"]
    #capturando el tipo seleccionado por el usuario
    provinciaSeleccionada=Provincia.objects.get(id_df=codigo_pro)
    nombreC_df=request.POST["nombreC_df"]
    apellidoC=request.POST["apellidoC"]
    direccionC_df=request.POST["direccionC_df"]
    correoC=request.POST["correoC"]
    fotografia=request.FILES.get("fotografia")

    #insertando datos mediante el ORM de Django
    nuevoCliente=Clientes.objects.create(nombreC_df=nombreC_df,apellidoC=apellidoC,direccionC_df=direccionC_df,correoC=correoC,fotografia=fotografia,provincia=provinciaSeleccionada)
    messages.success(request,'Cliente guardado exitosamente');
    return redirect('/clientes/')

def eliminarCliente(request,idC_df):
    clienteEliminar=Clientes.objects.get(idC_df=idC_df)
    clienteEliminar.delete()
    messages.success(request,'Eliminado Exitosamente');
    return redirect('/clientes/')

def editarCliente(request,idC_df):
    clientesEditar=Clientes.objects.get(idC_df=idC_df)
    provinciasBdd=Provincia.objects.all()
    return render(request,'editarClientes.html' ,{'cliente': clientesEditar,'provincias':provinciasBdd})


def procesarActualizacionClientes(request):
    idC_df=request.POST["idC_df"]
    codigo_pro=request.POST["codigo_pro"]
    provinciaSeleccionada=Provincia.objects.get(id_df=codigo_pro)
    nombreC_df=request.POST["nombreC_df"]
    apellidoC=request.POST["apellidoC"]
    direccionC_df=request.POST["direccionC_df"]
    correoC=request.POST["correoC"]

    if 'fotografia' in request.FILES:
            fotografia = request.FILES.get("fotografia")
    else:
            # Si no se proporciona una nueva imagen, conserva la existente
            cliente_existente = Clientes.objects.get(idC_df=idC_df)
            fotografia = cliente_existente.fotografia

    #Insertando datos mediante el ORM de DJANGO
    clienteEditar=Clientes.objects.get(idC_df=idC_df)
    clienteEditar.provincia=provinciaSeleccionada
    clienteEditar.nombreC_df=nombreC_df
    clienteEditar.apellidoC=apellidoC
    clienteEditar.direccionC_df=direccionC_df
    clienteEditar.correoC=correoC
    clienteEditar.fotografia=fotografia
    clienteEditar.save()
    messages.success(request,
      'Cliente ACTUALIZADO Exitosamente')
    return redirect('/clientes/')

    #Pedidos
def listaPedidos(request):
    pedidosBdd = Pedido.objects.all()
    clienteBdd=Clientes.objects.all()
    return render(request, 'listaPedidos.html', {'pedidos': pedidosBdd, 'clientes': clienteBdd})


def guardarPedido(request):
    #Capturando los valores del formulario
    id_cliente=request.POST["id_cliente"]
    #capturando el cliente seleccionado por el usuario
    #pedidoSeleccionado=Pedido.objects.get(idP_df=idP_df)
    clienteSeleccionado=Clientes.objects.get(idC_df=id_cliente)
    nombreP_df=request.POST["nombreP_df"]
    fecha_facP_df=request.POST["fecha_facP_df"]
    numero_facP_df=request.POST["numero_facP_df"]
    fotografia=request.FILES.get("fotografia")

    #insertando datos mediante el ORM de Django
    nuevoPedido=Pedido.objects.create(nombreP_df=nombreP_df,fecha_facP_df=fecha_facP_df,
    numero_facP_df=numero_facP_df,fotografia=fotografia, clientes=clienteSeleccionado)
    messages.success(request,'Pedido guardado exitosamente');
    return redirect('/pedidos/')

def eliminarPedido(request,idP_df):
    pedidoEliminar=Pedido.objects.get(idP_df=idP_df)
    pedidoEliminar.delete()
    messages.success(request,'Eliminado Exitosamente');
    return redirect('/pedidos/')

def editarPedido(request,idP_df):
    pedidoEditar=Pedido.objects.get(idP_df=idP_df)
    clienteBdd=Clientes.objects.all()
    return render(request,'editarPedidos.html' ,{'pedido': pedidoEditar, 'clientes': clienteBdd})


def procesarActualizacionPedidos(request):
    idP_df=request.POST["idP_df"]
    id_cliente=request.POST["id_cliente"]
    #pedidoSeleccionado=Pedido.objects.get(idP_df=idP_df)
    clienteSeleccionado=Clientes.objects.get(idC_df=idC_df)
    nombreP_df=request.POST["nombreP_df"]
    fecha_facP_df=request.POST["fecha_facP_df"]
    numero_facP_df=request.POST["numero_facP_df"]

    if 'fotografia' in request.FILES:
            fotografia = request.FILES.get("fotografia")
    else:
            # Si no se proporciona una nueva imagen, conserva la existente
            pedido_existente = Pedido.objects.get(idP_df=idP_df)
            fotografia = pedido_existente.fotografia

    #Insertando datos mediante el ORM de DJANGO
    pedidoEditar=Pedido.objects.get(idP_df=idP_df)
    pedidoEditar.cliente=clienteSeleccionado
    pedidoEditar.nombreP_df=nombreP_df
    pedidoEditar.fecha_facP_df=fecha_facP_df
    pedidoEditar.numero_facP_df=numero_facP_df
    pedidoEditar.fotografia=fotografia
    pedidoEditar.save()
    messages.success(request,
      'Pedido ACTUALIZADO Exitosamente')
    return redirect('/pedidos/')

    #tipo
def listaTipo(request):
    tiposBdd=Tipo.objects.all()
    return render(request,'listaTiposC.html',{'tipos':tiposBdd})


def guardarTipo(request):
    nombreT_df=request.POST["nombreT_df"]
    descripcionT_df=request.POST["descripcionT_df"]
    codigoT_df=request.POST["codigoT_df"]
    #insertando datos mediante el ORM de Django
    nuevoTipo=Tipo.objects.create(nombreT_df=nombreT_df,descripcionT_df=descripcionT_df,codigoT_df=codigoT_df)
    messages.success(request,'Tipo comida guardado exitosamente');
    return redirect('/tipos/')

def eliminarTipo(request,idT_df):
    tipoEliminar=Tipo.objects.get(idT_df=idT_df)
    tipoEliminar.delete()
    messages.success(request,'Eliminado Exitosamente');
    return redirect('/tipos/')

def editarTipo(request,idT_df):
    tipoEditar=Tipo.objects.get(idT_df=idT_df)
    return render(request,'editarTipoC.html' ,{'tipo': tipoEditar})


def procesarActualizacionTipo(request):
    idT_df=request.POST["idT_df"]
    nombreT_df=request.POST["nombreT_df"]
    descripcionT_df=request.POST["descripcionT_df"]
    codigoT_df=request.POST["codigoT_df"]
    #if 'fotografia' in request.FILES:
            #fotografia = request.FILES.get("fotografia")
    #else:
            # Si no se proporciona una nueva imagen, conserva la existente
            #provincia_existente = Provincia.objects.get(id_df=id_df)
            #fotografia = provincia_existente.fotografia

    #Insertando datos mediante el ORM de DJANGO
    tipoEditar=Tipo.objects.get(idT_df=idT_df)
    tipoEditar.nombreT_df=nombreT_df
    tipoEditar.descripcionT_df=descripcionT_df
    tipoEditar.codigoT_df=codigoT_df
    #provinciaEditar.fotografia=fotografia
    tipoEditar.save()
    messages.success(request,
      'Tipo comida ACTUALIZADA Exitosamente')
    return redirect('/tipos')

    #PLATILLOS
def listaPlatillos(request):
    platillosBdd=Platillo.objects.all()
    tiposBdd=Tipo.objects.all()
    return render(request,'listaPlatillos.html',{'platillos':platillosBdd, 'tipos':tiposBdd})


def guardarPlatillo(request):
    #Capturando los valores del formulario
    id_tipo=request.POST["id_tipo"]
    #capturando el tipo seleccionado por el usuario
    tipoSeleccionado=Tipo.objects.get(idT_df=id_tipo)
    nombrePl_df=request.POST["nombrePl_df"]
    descripcionPl_df=request.POST["descripcionPl_df"]
    precioPl_df=request.POST["precioPl_df"]
    fotografia=request.FILES.get("fotografia")

    #insertando datos mediante el ORM de Django
    nuevoPlatillo=Platillo.objects.create(nombrePl_df=nombrePl_df,descripcionPl_df=descripcionPl_df,precioPl_df=precioPl_df,fotografia=fotografia,tipo=tipoSeleccionado)
    messages.success(request,'Platillo guardado exitosamente');
    return redirect('/platillo/')

def eliminarPlatillo(request,idPl_df):
    platilloEliminar=Platillo.objects.get(idPl_df=idPl_df)
    platilloEliminar.delete()
    messages.success(request,'Eliminado Exitosamente');
    return redirect('/platillo/')

def editarPlatillo(request,idPl_df):
    platilloEditar=Platillo.objects.get(idPl_df=idPl_df)
    tiposBdd=Tipo.objects.all()
    return render(request,'editarPlatillos.html' ,{'platillos': platilloEditar,'tipos':tiposBdd})


def procesarActualizacionPlatillo(request):
    idPl_df=request.POST["idPl_df"]
    id_tipo=request.POST["id_tipo"]
    tipoSeleccionado=Tipo.objects.get(idT_df=idT_df)
    nombrePl_df=request.POST["nombrePl_df"]
    descripcionPl_df=request.POST["descripcionPl_df"]
    precioPl_df=request.POST["precioPl_df"]

    if 'fotografia' in request.FILES:
            fotografia = request.FILES.get("fotografia")
    else:
            # Si no se proporciona una nueva imagen, conserva la existente
            platillo_existente = Platillo.objects.get(idPl_df=idPl_df)
            fotografia = platillo_existente.fotografia

    #Insertando datos mediante el ORM de DJANGO
    platilloEditar=Platillo.objects.get(idPl_df=idPl_df)
    platilloEditar.tipo=tipoSeleccionado
    platilloEditar.nombrePl_df=nombrePl_df
    platilloEditar.descripcionPl_df=descripcionPl_df
    platilloEditar.precioPl_df=precioPl_df
    platilloEditar.fotografia=fotografia
    platilloEditar.save()
    messages.success(request,
      'Platillo ACTUALIZADO Exitosamente')
    return redirect('/platillo/')

    #Detalle
def listaDetalle(request):
    detallesBdd=Detalle.objects.all()
    pedidosBdd=Pedido.objects.all()
    platillosBdd=Platillo.objects.all()
    return render(request,'listaDetalle.html',{'detalles':detallesBdd,'pedidos':pedidosBdd,'platillos':platillosBdd})


def guardarDetalle(request):
    #Capturando los valores del formulario
    id_pedido=request.POST["id_pedido"]
    id_platillo=request.POST["id_platillo"]
    #capturando el tipo seleccionado por el usuario
    pedidoSeleccionado=Pedido.objects.get(idP_df=id_pedido)
    platilloSeleccionado=Platillo.objects.get(idPl_df=id_platillo)
    cantidadD_df=request.POST["cantidadD_df"]
    precioD_df=request.POST["precioD_df"]
    totalD_df=request.POST["totalD_df"]
    fotografia=request.FILES.get("fotografia")

    #insertando datos mediante el ORM de Django
    nuevoDetalle=Detalle.objects.create(cantidadD_df=cantidadD_df,precioD_df=precioD_df,totalD_df=totalD_df,fotografia=fotografia,pedido=pedidoSeleccionado,platillo=platilloSeleccionado)
    messages.success(request,'Detalle guardado exitosamente');
    return redirect('/detalles/')

def eliminarDetalle(request,idD_df):
    detalleEliminar=Detalle.objects.get(idD_df=idD_df)
    detalleEliminar.delete()
    messages.success(request,'Eliminado Exitosamente');
    return redirect('/detalles/')

def editarDetalle(request,idD_df):
    detalleEditar=Detalle.objects.get(idD_df=idD_df)
    pedidosBdd=Pedido.objects.all()
    platillosBdd=Platillo.objects.all()
    return render(request,'editarDetalle.html' ,{'detalle': detalleEditar,'pedidos':pedidosBdd,'platillos':platillosBdd})


def procesarActualizacionDetalle(request):
    idD_df=request.POST["idD_df"]
    id_pedido=request.POST["id_pedido"]
    id_platillo=request.POST["id_platillo"]
    pedidoSeleccionado=Pedido.objects.get(idP_df=id_pedido)
    platilloSeleccionado=Platillo.objects.get(idPl_df=id_platillo)
    cantidadD_df=request.POST["cantidadD_df"]
    precioD_df=request.POST["precioD_df"]
    totalD_df=request.POST["totalD_df"]

    if 'fotografia' in request.FILES:
            fotografia = request.FILES.get("fotografia")
    else:
            # Si no se proporciona una nueva imagen, conserva la existente
            detalle_existente = Detalle.objects.get(idD_df=idD_df)
            fotografia = detalle_existente.fotografia

    #Insertando datos mediante el ORM de DJANGO
    detalleEditar=Detalle.objects.get(idD_df=idD_df)
    detalleEditar.pedido=pedidoSeleccionado
    detalleEditar.platillo=platilloSeleccionado
    detalleEditar.cantidadD_df=cantidadD_df
    detalleEditar.precioD_df=precioD_df
    detalleEditar.totalD_df=totalD_df
    detalleEditar.fotografia=fotografia
    detalleEditar.save()
    messages.success(request,
      'Detalle ACTUALIZADO Exitosamente')
    return redirect('/detalles/')

    #Ingredientes
def listaIngredientes(request):
    ingredientesBdd=Ingredientes.objects.all()
    return render(request,'listaIngredientes.html',{'ingredientes':ingredientesBdd})


def guardarIngredientes(request):
    nombreI_df=request.POST["nombreI_df"]
    descripcionI_df=request.POST["descripcionI_df"]
    fecha_cadI_df=request.POST["fecha_cadI_df"]
    fotografia=request.FILES.get("fotografia")
    #insertando datos mediante el ORM de Django
    nuevoIngrediente=Ingredientes.objects.create(nombreI_df=nombreI_df,descripcionI_df=descripcionI_df,fecha_cadI_df=fecha_cadI_df, fotografia=fotografia)
    messages.success(request,'Ingrediente guardado exitosamente');
    return redirect('/ingredientes/')

def eliminarIngredientes(request,idI_df):
    ingredienteEliminar=Ingredientes.objects.get(idI_df=idI_df)
    ingredienteEliminar.delete()
    messages.success(request,'Eliminado Exitosamente');
    return redirect('/ingredientes/')

def editarIngredientes(request,idI_df):
    ingredienteEditar=Ingredientes.objects.get(idI_df=idI_df)
    ingredientesBdd=Ingredientes.objects.all()
    return render(request,'editarIngredientes.html' ,{'ingredientes': ingredienteEditar})


def procesarActualizacionIngredientes(request):
    idI_df=request.POST["idI_df"]
    nombreI_df=request.POST["nombreI_df"]
    descripcionI_df=request.POST["descripcionI_df"]
    fecha_cadI_df=request.POST["fecha_cadI_df"]
    if 'fotografia' in request.FILES:
            fotografia = request.FILES.get("fotografia")
    else:
            # Si no se proporciona una nueva imagen, conserva la existente
            ingrediente_existente = Ingredientes.objects.get(idI_df=idI_df)
            fotografia = ingrediente_existente.fotografia

    #Insertando datos mediante el ORM de DJANGO
    ingredienteEditar=Ingredientes.objects.get(idI_df=idI_df)
    ingredienteEditar.nombreI_df=nombreI_df
    ingredienteEditar.descripcionI_df=descripcionI_df
    ingredienteEditar.fecha_cadI_df=fecha_cadI_df
    ingredienteEditar.fotografia=fotografia
    ingredienteEditar.save()
    messages.success(request,
      'Ingrediente ACTUALIZADO Exitosamente')
    return redirect('/ingredientes/')

    #recetas
def listaRecetas(request):
    recetasBdd=Receta.objects.all()
    platillosBdd=Platillo.objects.all()
    ingredientesBdd=Ingredientes.objects.all()
    return render(request,'listaRecetas.html',{'recetas':recetasBdd,'platillos':platillosBdd,'ingredientes':ingredientesBdd})


def guardarReceta(request):
    #Capturando los valores del formulario
    id_platillo=request.POST["id_platillo"]
    id_ingredientes=request.POST["id_ingredientes"]
    #capturando el tipo seleccionado por el usuario
    platilloSeleccionado=Platillo.objects.get(idPl_df=id_platillo)
    ingredienteSeleccionado=Ingredientes.objects.get(idI_df=id_ingredientes)
    nombreR_df=request.POST["nombreR_df"]
    tiempopreR_df=request.POST["tiempopreR_df"]
    descripcionR_df=request.POST["descripcionR_df"]

    #insertando datos mediante el ORM de Django
    nuevaReceta=Receta.objects.create(nombreR_df=nombreR_df,tiempopreR_df=tiempopreR_df,descripcionR_df=descripcionR_df,platillo=platilloSeleccionado,ingredientes=ingredienteSeleccionado)
    messages.success(request,'Recetas guardado exitosamente');
    return redirect('/receta/')

def eliminarReceta(request,idR_df):
    recetaEliminar=Receta.objects.get(idR_df=idR_df)
    recetaEliminar.delete()
    messages.success(request,'Eliminado Exitosamente');
    return redirect('/receta/')

def editarReceta(request,idR_df):
    recetaEditar=Receta.objects.get(idR_df=idR_df)
    platillosBdd=Platillo.objects.all()
    ingredientesBdd=Ingredientes.objects.all()
    return render(request,'editarRecetas.html' ,{'receta': recetaEditar,'platillos':platillosBdd,'ingredientes':ingredientesBdd})


def procesarActualizacionReceta(request):
    idR_df=request.POST["idR_df"]
    id_platillo=request.POST["id_platillo"]
    id_ingredientes=request.POST["id_ingredientes"]
    platilloSeleccionado=Platillo.objects.get(idPl_df=id_platillo)
    ingredienteSeleccionado=Ingredientes.objects.get(idI_df=id_ingredientes)
    nombreR_df=request.POST["nombreR_df"]
    tiempopreR_df=request.POST["tiempopreR_df"]
    descripcionR_df=request.POST["descripcionR_df"]

    #if 'fotografia' in request.FILES:
            #fotografia = request.FILES.get("fotografia")
#    else:
            # Si no se proporciona una nueva imagen, conserva la existente
            #receta_existente = Receta.objects.get(idR_df=idR_df)
            #fotografia = receta_existente.fotografia

    #Insertando datos mediante el ORM de DJANGO
    recetaEditar=Receta.objects.get(idR_df=idR_df)
    recetaEditar.platillo=platilloSeleccionado
    recetaEditar.ingredientes=ingredienteSeleccionado
    recetaEditar.nombreR_df=nombreR_df
    recetaEditar.tiempopreR_df=tiempopreR_df
    recetaEditar.descripcionR_df=descripcionR_df
    recetaEditar.save()
    messages.success(request,
      'Receta ACTUALIZADA Exitosamente')
    return redirect('/receta/')
