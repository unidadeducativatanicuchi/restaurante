from django.apps import AppConfig


class RestaurantedfConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Aplicaciones.RestauranteDF'
