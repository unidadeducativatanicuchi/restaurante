from django.urls import path
from . import views

urlpatterns=[
    path('',views.listadoProvincia),
    path('guardarProvincia/',views.guardarProvincia),
    path('eliminarProvincia/<id_df>',views.eliminarProvincia),
    path('editarProvincia/<id_df>',views.editarProvincia),
    path('procesarActualizacionProvincia/',views.procesarActualizacionProvincia),

    path('clientes/',views.listaClientes, name='clientes'),
    path('guardarCliente/',views.guardarCliente),
    path('eliminarCliente/<idC_df>',views.eliminarCliente),
    path('editarCliente/<idC_df>',views.editarCliente),
    path('procesarActualizacionClientes/',views.procesarActualizacionClientes),

    path('pedidos/',views.listaPedidos, name='pedidos'),
    path('guardarPedido/',views.guardarPedido),
    path('eliminarPedido/<idP_df>',views.eliminarPedido),
    path('editarPedido/<idP_df>',views.editarPedido),
    path('procesarActualizacionPedidos/',views.procesarActualizacionPedidos),

    path('tipos/',views.listaTipo, name='tipos'),
    path('guardarTipo/',views.guardarTipo),
    path('eliminarTipo/<idT_df>',views.eliminarTipo),
    path('editarTipo/<idT_df>',views.editarTipo),
    path('procesarActualizacionTipo/',views.procesarActualizacionTipo),

    path('platillo/',views.listaPlatillos, name='platillo'),
    path('guardarPlatillo/',views.guardarPlatillo),
    path('eliminarPlatillo/<idPl_df>',views.eliminarPlatillo),
    path('editarPlatillo/<idPl_df>',views.editarPlatillo),
    path('procesarActualizacionPlatillo/',views.procesarActualizacionPlatillo),

    path('detalles/',views.listaDetalle, name='detalles'),
    path('guardarDetalle/',views.guardarDetalle),
    path('eliminarDetalle/<idD_df>',views.eliminarDetalle),
    path('editarDetalle/<idD_df>',views.editarDetalle),
    path('procesarActualizacionDetalle/',views.procesarActualizacionDetalle),

    path('ingredientes/',views.listaIngredientes, name='ingredientes'),
    path('guardarIngredientes/',views.guardarIngredientes),
    path('eliminarIngredientes/<idI_df>',views.eliminarIngredientes),
    path('editarIngredientes/<idI_df>',views.editarIngredientes),
    path('procesarActualizacionIngredientes/',views.procesarActualizacionIngredientes),

    path('receta/',views.listaRecetas, name='receta'),
    path('guardarReceta/',views.guardarReceta),
    path('eliminarReceta/<idR_df>',views.eliminarReceta),
    path('editarReceta/<idR_df>',views.editarReceta),
    path('procesarActualizacionReceta/',views.procesarActualizacionReceta),

]
