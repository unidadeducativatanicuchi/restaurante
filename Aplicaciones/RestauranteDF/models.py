from django.db import models

# Create your models here.
class Provincia(models.Model):
    id_df=models.AutoField(primary_key=True)
    regionPr_df=models.CharField(max_length=150)
    nombrePr_df=models.CharField(max_length=150)
    capitalPr_df=models.CharField(max_length=150)
    fotografia=models.FileField(upload_to='provincia', null=True,blank=True)

    def __str__(self):
        fila="{0}: {1} - {2}"
        return fila.format(self.regionPr_df, self.nombrePr_df, self.capitalPr_df)

class Clientes(models.Model):
    idC_df=models.AutoField(primary_key=True)
    nombreC_df=models.CharField(max_length=150)
    apellidoC=models.CharField(max_length=150)
    direccionC_df=models.CharField(max_length=150)
    correoC=models.EmailField()
    provincia=models.ForeignKey(Provincia, null=True,blank=True,on_delete=models.CASCADE)
    fotografia=models.FileField(upload_to='clientes', null=True,blank=True)
    def __str__(self):
        fila="{0}: {1} {2} - {3}"
        return fila.format(self.nombreC_df, self.apellidoC, self.direccionC_df, self.correoC)

class Pedido(models.Model):
    idP_df=models.AutoField(primary_key=True)
    nombreP_df=models.CharField(max_length=150)
    fecha_facP_df=models.DateField()
    numero_facP_df=models.CharField(max_length=150)
    clientes=models.ForeignKey(Clientes, null=True,blank=True,on_delete=models.CASCADE)
    fotografia=models.FileField(upload_to='pedidos', null=True,blank=True)
    def __str__(self):
        fila="{0}: {1} - {2}"
        return fila.format(self.nombreP_df, self.fecha_facP_df, self.numero_facP_df)

class Tipo(models.Model):
    idT_df=models.AutoField(primary_key=True)
    nombreT_df=models.CharField(max_length=150)
    descripcionT_df=models.TextField(max_length=150)
    codigoT_df=models.CharField(max_length=150)
    def __str__(self):
        fila="{0}: {1} - {2}"
        return fila.format(self.nombreT_df, self.descripcionT_df, self.codigoT_df)

class Platillo(models.Model):
    idPl_df=models.AutoField(primary_key=True)
    nombrePl_df=models.CharField(max_length=150)
    descripcionPl_df=models.TextField()
    precioPl_df=models.CharField(max_length=150)
    tipo=models.ForeignKey(Tipo, null=True,blank=True,on_delete=models.CASCADE)
    fotografia=models.FileField(upload_to='platillos', null=True,blank=True)
    def __str__(self):
        fila="{0}: {1} {2}"
        return fila.format(self.nombrePl_df, self.descripcionPl_df, self.precioPl_df)

class Detalle(models.Model):
    idD_df=models.AutoField(primary_key=True)
    cantidadD_df=models.CharField(max_length=150)
    precioD_df=models.CharField(max_length=150)
    totalD_df=models.CharField(max_length=150)
    pedido=models.ForeignKey(Pedido, null=True,blank=True,on_delete=models.CASCADE)
    platillo=models.ForeignKey(Platillo, null=True,blank=True,on_delete=models.CASCADE)
    fotografia=models.FileField(upload_to='platillos', null=True,blank=True)

    def __str__(self):
        fila="{0}: {1} - {2}"
        return fila.format(self.cantidadD_df, self.precioD_df, self.totalD_df, self.fotografia)

class Ingredientes(models.Model):
     idI_df=models.AutoField(primary_key=True)
     nombreI_df=models.CharField(max_length=150)
     descripcionI_df=models.CharField(max_length=150)
     fecha_cadI_df=models.DateField()
     fotografia=models.FileField(upload_to='ingredientes', null=True,blank=True)
     def __str__(self):
         fila="{0}: {1} - {2}"
         return fila.format(self.nombreI_df, self.descripcionI_df, self.fecha_cadI_df)

class Receta(models.Model):
    idR_df=models.AutoField(primary_key=True)
    nombreR_df=models.CharField(max_length=150)
    tiempopreR_df=models.CharField(max_length=150)
    descripcionR_df=models.CharField(max_length=150)
    platillo=models.ForeignKey(Platillo, null=True,blank=True,on_delete=models.CASCADE)
    ingredientes=models.ForeignKey(Ingredientes, null=True,blank=True,on_delete=models.CASCADE)
    def __str__(self):
        fila="{0}: {1} - {2}"
        return fila.format(self.nombreR_df, self.tiempopreR_df, self.descripcionR_df)
