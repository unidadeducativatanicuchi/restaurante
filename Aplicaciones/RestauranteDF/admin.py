from django.contrib import admin
from.models import Provincia
from.models import Clientes
from.models import Pedido
from.models import Tipo
from.models import Platillo
from.models import Detalle
from.models import Ingredientes
from.models import Receta

# Register your models here.
admin.site.register(Provincia)
admin.site.register(Clientes)
admin.site.register(Pedido)
admin.site.register(Tipo)
admin.site.register(Platillo)
admin.site.register(Detalle)
admin.site.register(Ingredientes)
admin.site.register(Receta)
